<?php

namespace Ucinf\PaperlitBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Ucinf\PaperlitBundle\Entity\Suscriptor;
use Ucinf\PaperlitBundle\Form\SuscriptorType;

/**
 * Suscriptor controller.
 *
 */
class SuscriptorController extends Controller
{

    /**
     * Lists all Suscriptor entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('PaperlitBundle:Suscriptor')->findAll();

        return $this->render('PaperlitBundle:Suscriptor:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Suscriptor entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Suscriptor();
        $form = $this->createForm(new SuscriptorType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('suscriptor_show', array('id' => $entity->getId())));
        }

        return $this->render('PaperlitBundle:Suscriptor:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Suscriptor entity.
     *
     */
    public function newAction()
    {
        $entity = new Suscriptor();
        $form   = $this->createForm(new SuscriptorType(), $entity);

        return $this->render('PaperlitBundle:Suscriptor:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Suscriptor entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PaperlitBundle:Suscriptor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Suscriptor entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PaperlitBundle:Suscriptor:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Suscriptor entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PaperlitBundle:Suscriptor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Suscriptor entity.');
        }

        $editForm = $this->createForm(new SuscriptorType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PaperlitBundle:Suscriptor:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Suscriptor entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PaperlitBundle:Suscriptor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Suscriptor entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new SuscriptorType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('suscriptor_edit', array('id' => $id)));
        }

        return $this->render('PaperlitBundle:Suscriptor:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Suscriptor entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('PaperlitBundle:Suscriptor')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Suscriptor entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('suscriptor'));
    }

    /**
     * Creates a form to delete a Suscriptor entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
