<?php

namespace Ucinf\PaperlitBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Ucinf\PaperlitBundle\Entity\Suscripcion;
use Ucinf\PaperlitBundle\Form\SuscripcionType;

/**
 * Suscripcion controller.
 *
 */
class SuscripcionController extends Controller
{

    /**
     * Lists all Suscripcion entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('PaperlitBundle:Suscripcion')->findAll();

        return $this->render('PaperlitBundle:Suscripcion:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Suscripcion entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Suscripcion();
        $form = $this->createForm(new SuscripcionType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('suscripcion_show', array('id' => $entity->getId())));
        }

        return $this->render('PaperlitBundle:Suscripcion:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Suscripcion entity.
     *
     */
    public function newAction()
    {
        $entity = new Suscripcion();
        $form   = $this->createForm(new SuscripcionType(), $entity);

        return $this->render('PaperlitBundle:Suscripcion:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Suscripcion entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PaperlitBundle:Suscripcion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Suscripcion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PaperlitBundle:Suscripcion:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Suscripcion entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PaperlitBundle:Suscripcion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Suscripcion entity.');
        }

        $editForm = $this->createForm(new SuscripcionType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PaperlitBundle:Suscripcion:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Suscripcion entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PaperlitBundle:Suscripcion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Suscripcion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new SuscripcionType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('suscripcion_edit', array('id' => $id)));
        }

        return $this->render('PaperlitBundle:Suscripcion:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Suscripcion entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('PaperlitBundle:Suscripcion')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Suscripcion entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('suscripcion'));
    }

    /**
     * Creates a form to delete a Suscripcion entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
