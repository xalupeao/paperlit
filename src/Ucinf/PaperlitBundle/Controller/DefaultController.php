<?php

namespace Ucinf\PaperlitBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {

        return $this->render('PaperlitBundle:Default:index.html.twig');
    }
}
