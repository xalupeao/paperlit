<?php

namespace Ucinf\PaperlitBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Ucinf\PaperlitBundle\Entity\Editorial;
use Ucinf\PaperlitBundle\Form\EditorialType;

/**
 * Editorial controller.
 *
 */
class EditorialController extends Controller
{

    /**
     * Lists all Editorial entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('PaperlitBundle:Editorial')->findAll();

        return $this->render('PaperlitBundle:Editorial:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Editorial entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Editorial();
        $form = $this->createForm(new EditorialType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('editorial_show', array('id' => $entity->getId())));
        }

        return $this->render('PaperlitBundle:Editorial:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Editorial entity.
     *
     */
    public function newAction()
    {
        $entity = new Editorial();
        $form   = $this->createForm(new EditorialType(), $entity);

        return $this->render('PaperlitBundle:Editorial:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Editorial entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PaperlitBundle:Editorial')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Editorial entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PaperlitBundle:Editorial:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Editorial entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PaperlitBundle:Editorial')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Editorial entity.');
        }

        $editForm = $this->createForm(new EditorialType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PaperlitBundle:Editorial:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Editorial entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PaperlitBundle:Editorial')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Editorial entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new EditorialType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('editorial_edit', array('id' => $id)));
        }

        return $this->render('PaperlitBundle:Editorial:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Editorial entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('PaperlitBundle:Editorial')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Editorial entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('editorial'));
    }

    /**
     * Creates a form to delete a Editorial entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
