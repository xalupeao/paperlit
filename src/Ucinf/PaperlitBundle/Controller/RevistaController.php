<?php

namespace Ucinf\PaperlitBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Ucinf\PaperlitBundle\Entity\Revista;
use Ucinf\PaperlitBundle\Form\RevistaType;

/**
 * Revista controller.
 *
 */
class RevistaController extends Controller
{

    /**
     * Lists all Revista entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('PaperlitBundle:Revista')->findAll();

        return $this->render('PaperlitBundle:Revista:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Revista entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Revista();
        $form = $this->createForm(new RevistaType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('revista_show', array('id' => $entity->getId())));
        }

        return $this->render('PaperlitBundle:Revista:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Revista entity.
     *
     */
    public function newAction()
    {
        $entity = new Revista();
        $form   = $this->createForm(new RevistaType(), $entity);

        return $this->render('PaperlitBundle:Revista:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Revista entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PaperlitBundle:Revista')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Revista entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PaperlitBundle:Revista:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Revista entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PaperlitBundle:Revista')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Revista entity.');
        }

        $editForm = $this->createForm(new RevistaType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PaperlitBundle:Revista:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Revista entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PaperlitBundle:Revista')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Revista entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new RevistaType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('revista_edit', array('id' => $id)));
        }

        return $this->render('PaperlitBundle:Revista:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Revista entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('PaperlitBundle:Revista')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Revista entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('revista'));
    }

    /**
     * Creates a form to delete a Revista entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
