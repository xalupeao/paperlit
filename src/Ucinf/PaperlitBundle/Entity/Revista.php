<?php

namespace Ucinf\PaperlitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Revista
 *
 * @ORM\Table(name="magazines")
 * @ORM\Entity(repositoryClass="Ucinf\PaperlitBundle\Entity\RevistaRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Revista
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=200)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="Ucinf\PaperlitBundle\Entity\Editorial", inversedBy="magazines")
     * @ORM\JoinColumn(name="editorial_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    protected $editorial;

    /**
     * @ORM\OneToMany(targetEntity="Ucinf\PaperlitBundle\Entity\Suscripcion", mappedBy="magazine")
     */
    protected $suscriptions;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->suscriptions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Revista
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Revista
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Revista
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set editorial
     *
     * @param \Ucinf\PaperlitBundle\Entity\Editorial $editorial
     * @return Revista
     */
    public function setEditorial(\Ucinf\PaperlitBundle\Entity\Editorial $editorial = null)
    {
        $this->editorial = $editorial;

        return $this;
    }

    /**
     * Get editorial
     *
     * @return \Ucinf\PaperlitBundle\Entity\Editorial
     */
    public function getEditorial()
    {
        return $this->editorial;
    }

    /**
     * Add suscriptions
     *
     * @param \Ucinf\PaperlitBundle\Entity\Suscripcion $suscriptions
     * @return Revista
     */
    public function addSuscription(\Ucinf\PaperlitBundle\Entity\Suscripcion $suscriptions)
    {
        $this->suscriptions[] = $suscriptions;

        return $this;
    }

    /**
     * Remove suscriptions
     *
     * @param \Ucinf\PaperlitBundle\Entity\Suscripcion $suscriptions
     */
    public function removeSuscription(\Ucinf\PaperlitBundle\Entity\Suscripcion $suscriptions)
    {
        $this->suscriptions->removeElement($suscriptions);
    }

    /**
     * Get suscriptions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSuscriptions()
    {
        return $this->suscriptions;
    }

    /**
     * toString Method
     * @return string name user
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Now we tell doctrine that before we persist or update we call the updatedTimestamps() function.
     * @link http://www.jamesmandrews.com/2012/10/31/symfony-2-entity-timestamps-with-doctrine/
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime(date('Y-m-d H:i:s')));

        if($this->getCreatedAt() == null)
        {
            $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        }
    }
}