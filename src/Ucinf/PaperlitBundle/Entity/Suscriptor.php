<?php

namespace Ucinf\PaperlitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Suscriptor
 *
 * @ORM\Table(name="subscribers")
 * @ORM\Entity(repositoryClass="Ucinf\PaperlitBundle\Entity\SuscriptorRepository")7
 * @ORM\HasLifecycleCallbacks
 */
class Suscriptor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=150)
     * @Assert\NotBlank()
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=200)
     * @Assert\NotBlank()
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=200)
     * @Assert\NotBlank()
     * @Assert\Email(
     *     checkMX = true,
     *     checkHost = true
     * )
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=20)
     * @Assert\NotBlank()
     */
    private $number;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="Ucinf\PaperlitBundle\Entity\Suscripcion", mappedBy="subscriber")
     */
    protected $suscriptions;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->suscriptions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return Suscriptor
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Suscriptor
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Suscriptor
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Suscriptor
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set number
     *
     * @param string $number
     * @return Suscriptor
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Suscriptor
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Suscriptor
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add suscriptions
     *
     * @param \Ucinf\PaperlitBundle\Entity\Suscripcion $suscriptions
     * @return Suscriptor
     */
    public function addSuscription(\Ucinf\PaperlitBundle\Entity\Suscripcion $suscriptions)
    {
        $this->suscriptions[] = $suscriptions;

        return $this;
    }

    /**
     * Remove suscriptions
     *
     * @param \Ucinf\PaperlitBundle\Entity\Suscripcion $suscriptions
     */
    public function removeSuscription(\Ucinf\PaperlitBundle\Entity\Suscripcion $suscriptions)
    {
        $this->suscriptions->removeElement($suscriptions);
    }

    /**
     * Get suscriptions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSuscriptions()
    {
        return $this->suscriptions;
    }

    /**
     * toString Method
     * @return string name user
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Now we tell doctrine that before we persist or update we call the updatedTimestamps() function.
     * @link http://www.jamesmandrews.com/2012/10/31/symfony-2-entity-timestamps-with-doctrine/
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime(date('Y-m-d H:i:s')));

        if($this->getCreatedAt() == null)
        {
            $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        }
    }
}