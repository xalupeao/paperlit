<?php

namespace Ucinf\PaperlitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Editorial
 *
 * @ORM\Table(name="editorials")
 * @ORM\Entity(repositoryClass="Ucinf\PaperlitBundle\Entity\EditorialRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Editorial
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text")
     * @Assert\NotBlank()
     */
    private $address;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="Ucinf\PaperlitBundle\Entity\Usuario", mappedBy="editorial")
     */
    protected $users;

    /**
     * @ORM\OneToMany(targetEntity="Ucinf\PaperlitBundle\Entity\Revista", mappedBy="editorial")
     */
    protected $magazines;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
        $this->magazines = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Editorial
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Editorial
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Editorial
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Editorial
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add users
     *
     * @param \Ucinf\PaperlitBundle\Entity\Usuario $users
     * @return Editorial
     */
    public function addUser(\Ucinf\PaperlitBundle\Entity\Usuario $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \Ucinf\PaperlitBundle\Entity\Usuario $users
     */
    public function removeUser(\Ucinf\PaperlitBundle\Entity\Usuario $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add magazines
     *
     * @param \Ucinf\PaperlitBundle\Entity\Revista $magazines
     * @return Editorial
     */
    public function addMagazine(\Ucinf\PaperlitBundle\Entity\Revista $magazines)
    {
        $this->magazines[] = $magazines;

        return $this;
    }

    /**
     * Remove magazines
     *
     * @param \Ucinf\PaperlitBundle\Entity\Revista $magazines
     */
    public function removeMagazine(\Ucinf\PaperlitBundle\Entity\Revista $magazines)
    {
        $this->magazines->removeElement($magazines);
    }

    /**
     * Get magazines
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMagazines()
    {
        return $this->magazines;
    }

    /**
     * toString Method
     * @return string name user
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Now we tell doctrine that before we persist or update we call the updatedTimestamps() function.
     * @link http://www.jamesmandrews.com/2012/10/31/symfony-2-entity-timestamps-with-doctrine/
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime(date('Y-m-d H:i:s')));

        if($this->getCreatedAt() == null)
        {
            $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        }
    }
}