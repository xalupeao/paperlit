<?php

namespace Ucinf\PaperlitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Suscripcion
 *
 * @ORM\Table(name="suscriptions")
 * @ORM\Entity(repositoryClass="Ucinf\PaperlitBundle\Entity\SuscripcionRepository")
 */
class Suscripcion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="begin_date", type="date")
     * @Assert\NotBlank()
     * @Assert\Date()
     */
    private $beginDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="date")
     * @Assert\NotBlank()
     * @Assert\Date()
     */
    private $endDate;

    /**
     * @ORM\ManyToOne(targetEntity="Ucinf\PaperlitBundle\Entity\Revista", inversedBy="suscriptions")
     * @ORM\JoinColumn(name="magazine_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    protected $magazine;

    /**
     * @ORM\ManyToOne(targetEntity="Ucinf\PaperlitBundle\Entity\Suscriptor", inversedBy="suscriptions")
     * @ORM\JoinColumn(name="subscriber_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    protected $subscriber;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set beginDate
     *
     * @param \DateTime $beginDate
     * @return Suscripcion
     */
    public function setBeginDate($beginDate)
    {
        $this->beginDate = $beginDate;

        return $this;
    }

    /**
     * Get beginDate
     *
     * @return \DateTime
     */
    public function getBeginDate()
    {
        return $this->beginDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return Suscripcion
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set magazine
     *
     * @param \Ucinf\PaperlitBundle\Entity\Revista $magazine
     * @return Suscripcion
     */
    public function setMagazine(\Ucinf\PaperlitBundle\Entity\Revista $magazine = null)
    {
        $this->magazine = $magazine;

        return $this;
    }

    /**
     * Get magazine
     *
     * @return \Ucinf\PaperlitBundle\Entity\Revista
     */
    public function getMagazine()
    {
        return $this->magazine;
    }

    /**
     * Set subscriber
     *
     * @param \Ucinf\PaperlitBundle\Entity\Suscriptor $subscriber
     * @return Suscripcion
     */
    public function setSubscriber(\Ucinf\PaperlitBundle\Entity\Suscriptor $subscriber = null)
    {
        $this->subscriber = $subscriber;

        return $this;
    }

    /**
     * Get subscriber
     *
     * @return \Ucinf\PaperlitBundle\Entity\Suscriptor
     */
    public function getSubscriber()
    {
        return $this->subscriber;
    }
}