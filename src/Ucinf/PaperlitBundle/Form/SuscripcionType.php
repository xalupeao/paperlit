<?php

namespace Ucinf\PaperlitBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SuscripcionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('beginDate', 'date', array(
                'label' => 'Fecha de Inicio',
            ))
            ->add('endDate', 'date', array(
                'label' => 'Fecha de Termino',
            ))
            ->add('magazine')
            ->add('subscriber')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ucinf\PaperlitBundle\Entity\Suscripcion'
        ));
    }

    public function getName()
    {
        return 'ucinf_paperlitbundle_suscripciontype';
    }
}
