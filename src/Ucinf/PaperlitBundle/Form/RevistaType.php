<?php

namespace Ucinf\PaperlitBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RevistaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                'label' => 'Nombre'
            ))
            //->add('createdAt')
            //->add('updatedAt')
            ->add('editorial')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ucinf\PaperlitBundle\Entity\Revista'
        ));
    }

    public function getName()
    {
        return 'ucinf_paperlitbundle_revistatype';
    }
}
