<?php

namespace Ucinf\PaperlitBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UsuarioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', 'text', array(
                'label' => 'Nombre de Usuario'
            ))
            ->add('password', 'password', array(
                'label' => 'Contraseña'
            ))
            ->add('name', 'text', array(
                'label' => 'Nombre'
            ))
            ->add('email')
            //->add('createdAt')
            //->add('updatedAt')
            ->add('editorial')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ucinf\PaperlitBundle\Entity\Usuario'
        ));
    }

    public function getName()
    {
        return 'ucinf_paperlitbundle_usuariotype';
    }
}
