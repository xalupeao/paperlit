<?php

namespace Ucinf\PaperlitBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EditorialType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                'label' => 'Nombre'
            ))
            ->add('address', 'textarea', array(
                'label' => 'Dirección'
            ))
            //->add('createdAt')
            //->add('updatedAt')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ucinf\PaperlitBundle\Entity\Editorial'
        ));
    }

    public function getName()
    {
        return 'ucinf_paperlitbundle_editorialtype';
    }
}
