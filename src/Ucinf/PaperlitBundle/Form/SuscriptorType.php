<?php

namespace Ucinf\PaperlitBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SuscriptorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', 'text', array(
                'label' => 'Nombre de Usuario',
            ))
            ->add('password', 'password', array(
                'label' => 'Contraseña',
            ))
            ->add('name', 'text', array(
                'label' => 'Nombre'
            ))
            ->add('email', 'email')
            ->add('number', 'text', array(
                'label' => 'Numero de Cliente',
            ))
            //->add('createdAt')
            //->add('updatedAt')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ucinf\PaperlitBundle\Entity\Suscriptor'
        ));
    }

    public function getName()
    {
        return 'ucinf_paperlitbundle_suscriptortype';
    }
}
